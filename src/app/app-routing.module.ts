import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AppHomeComponent} from "./components/home/app-home.component";
import {AppPoliciesComponent} from "./components/policies/app-policies.component";

const appRoutes: Routes = [
  {path: '', component: AppHomeComponent},
  {path: 'policies', component: AppPoliciesComponent}
];


@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}

