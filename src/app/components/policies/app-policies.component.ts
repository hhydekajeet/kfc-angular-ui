import { Component } from '@angular/core';
import {appProperties} from '../../../assets/app-properties.json';

@Component({
  selector: 'app-policies',
  templateUrl: './app-policies.component.html',
  styleUrls: ['./app-policies.component.css']
})
export class AppPoliciesComponent {
  title = 'KfcUi';
}
