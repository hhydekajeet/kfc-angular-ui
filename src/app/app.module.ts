import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from "@angular/forms";
import {Routes, Router, RouterModule} from '@angular/router';
import {HttpClientModule} from "@angular/common/http";

import { AppComponent } from './app.component';
import {AppTitleComponent} from "./components/app-title/app-title.component";
import {AppRoutingModule} from "./app-routing.module";
import {AppHomeComponent} from "./components/home/app-home.component";
import {AppPoliciesComponent} from "./components/policies/app-policies.component";

@NgModule({
  declarations: [
    AppComponent,
    AppTitleComponent,
    AppHomeComponent,
    AppPoliciesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
